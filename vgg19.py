from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

import torch
import torch.optim as optim
from torchvision import transforms, models

###################################################################################### Load the VGG model
vgg = models.vgg19(pretrained=True).features

for param in vgg.parameters():
    param.requires_grad_(False)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

vgg.to(device)


####################################################################################### Load conten and style images

def load_image(img_path, max_size=400, shape=None):
    ''' load in and trandorm an image,
        make sure the image is <= 400 pixels in the x-y dims.
    '''
    image = Image.open(img_path).convert('RGB')

    if max(image.size) > max_size:
        size = max_size
    else:
        size = max(image.size)

    if shape is not None:
        size = shape

    in_transform = transforms.Compose([
                        transforms.Resize(size),
                        transforms.ToTensor(),
                        transforms.Normalize((0.485,0.456,0.406),
                                             (0.229,0.224,0.225))
                        ])
    image = in_transform(image)[:3,:,:].unsqueeze(0)

    return image




content = load_image('images/content.jpg').to(device)

style = load_image('images/style.jpg', shape = content.shape[-2:]).to(device)



########################################################################################3 Image visualizer

def im_convert(tensor):
    ''' Diplay a tensor as an image'''

    image = tensor.to('cpu').clone().detach()
    image = image.numpy().squeeze()
    image = image.transpose(1,2,0)
    image = image*np.array((0.229,0.224,0.225)) + np.array((0.485,0.456,0.406))
    image = image.clip(0,1)

    return image
############################################################################################ show OG images

fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (20,10))

ax1.imshow(im_convert(content))
ax2.imshow(im_convert(style))

##########################33################################################################## get features

def get_features(image, model, layers = None):

    ''' Run an image forward through a model and get the features for a set of layers. Default layers are for VGGNet
    matching gatys et al (2016)
    '''

    if layers is None:
        layers = {'0': 'conv1_1',
                  '5': 'conv2_1',
                  '10': 'conv3_1',
                  '19': 'conv4_1',
                  '21': 'conv4_2',  #content representation
                  '28': 'conv5_1',
        }

    features = {}

    x = image

    for name, layer in model._modules.items():
        x = layer(x)
        if name in layers:
            features[layers[name]] = x

    return features

######################################################################## Gram Matrix


def gram_matrix(tensor):

    _, d, h, w = tensor.size()

    tensor = tensor.view(d,h*w)

    gram = torch.mm(tensor, tensor.t())

    return gram


############################################################################# Run

content_features = get_features(content,vgg)
style_features = get_features(style, vgg)

style_grams = {layer: gram_matrix(style_features[layer]) for layer in style_features}


target = content.clone().requires_grad_(True).to(device)


############################################################################### Loss and weights

style_weights = {'conv1_1' : 1.,
                 'conv2_1': 0.75,
                 'conv3_1': 0.2,
                 'conv4_1': 0.2,
                 'conv5_1': 0.2
                 }

content_weight = 1

style_weight = 1e6

####################################################################################### updating target & calculating losses

optimizer = optim.Adam([target], lr = 0.003)
steps = 2000

for ii in range(1, steps+1):
    target_features = get_features(target, vgg)

    content_loss = torch.mean((target_features['conv4_2'] - content_features['conv4_2']**2))

    style_loss = 0

    for layer in style_weights:

        target_feature = target_features[layer]
        target_gram = gram_matrix(target_feature)
        _,d,h,w = target_feature.shape

        style_gram = style_grams[layer]

        layer_style_loss = style_weights[layer]*torch.mean((target_gram - style_gram)**2)

        style_loss += layer_style_loss / (d*h*w)

    total_loss = content_weight* content_loss + style_weight * style_loss

    optimizer.zero_grad()
    total_loss.backward()
    optimizer.step()


###########################################################################Display


fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (20,10))

ax1.imshow(im_convert(content))
ax2.imshow(im_convert(target))

fig.savefig('./images/out.png')
