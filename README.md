# Image Artistic Syle Transfer

Transfers the style of one image onto another image using the VGG19 Convolution Neural Network feature extractor. This is based on: https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Gatys_Image_Style_Transfer_CVPR_2016_paper.pdf


can demo this at: debbagh.xyz/style_tranfer

*Note that it may take ~ 1-5 minutes depending on image complexicity so be patient!*