import torch
import torch.optim as optim
from torchvision import transforms, models



def initiate_model(model_arch ='vgg19'):

    vgg19 = models.vgg19(pretrained=True).features


    model_dict = {'vgg19': vgg19}
    model = model_dict[model_arch]

    return model

def get_features(image, model, layers = None):

    ''' Run an image forward through a model and get the features for a set of layers. Default layers are for VGGNet
    matching gatys et al (2016)
    '''

    if layers is None:
        layers = {'0': 'conv1_1',
                  '5': 'conv2_1',
                  '10': 'conv3_1',
                  '19': 'conv4_1',
                  '21': 'conv4_2',  #content representation
                  '28': 'conv5_1',
        }

    features = {}

    x = image

    for name, layer in model._modules.items():
        x = layer(x)
        if name in layers:
            features[layers[name]] = x

    return features

######################################################################## Gram Matrix


def gram_matrix(tensor):

    _, d, h, w = tensor.size()

    tensor = tensor.view(d,h*w)

    gram = torch.mm(tensor, tensor.t())

    return gram
