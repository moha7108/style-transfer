import cl_args
import CNN_model
import image_in_out
import torch
import torch.optim as optim
from torchvision import transforms, models
import matplotlib.pyplot as plt


args = cl_args.get_args()

device = torch.device("cuda" if args.gpu & torch.cuda.is_available() else "cpu")


model = CNN_model.initiate_model()
for param in model.parameters():
    param.requires_grad_(False)
model.to(device)


content_image = image_in_out.load_image(args.content_image_path).to(device)
style_image = image_in_out.load_image(args.style_image_path, shape = content_image.shape[-2:]).to(device)


content_features = CNN_model.get_features(content_image, model)
style_features = CNN_model.get_features(style_image, model)

style_grams = {layer: CNN_model.gram_matrix(style_features[layer]) for layer in style_features}


target = content_image.clone().requires_grad_(True).to(device)


style_weights = {'conv1_1' : 1.,
                 'conv2_1': 0.75,
                 'conv3_1': 0.2,
                 'conv4_1': 0.2,
                 'conv5_1': 0.2
                 }

content_weight = 1

style_weight = 1e6

####################################################################################### updating target & calculating losses

optimizer = optim.Adam([target], lr = 0.003)
steps = 2000

for ii in range(1, steps+1):
    target_features = CNN_model.get_features(target, model)

    content_loss = torch.mean((target_features['conv4_2'] - content_features['conv4_2']**2))

    style_loss = 0

    for layer in style_weights:

        target_feature = target_features[layer]
        target_gram = CNN_model.gram_matrix(target_feature)
        _,d,h,w = target_feature.shape

        style_gram = style_grams[layer]

        layer_style_loss = style_weights[layer]*torch.mean((target_gram - style_gram)**2)

        style_loss += layer_style_loss / (d*h*w)

    total_loss = content_weight* content_loss + style_weight * style_loss

    optimizer.zero_grad()
    total_loss.backward()
    optimizer.step()


fig, (ax) = plt.subplots(1, 1, figsize = (20,10))

ax.imshow(image_in_out.im_convert(target))

fig.savefig(args.output_image_path)
